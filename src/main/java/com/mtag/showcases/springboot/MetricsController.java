package com.mtag.showcases.springboot;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class MetricsController {
    private final Counter demoCounter;

    @Autowired
    private MeterRegistry registry;

    public MetricsController(MeterRegistry registry) {
        this.registry = registry;
        this.demoCounter = registry.counter("demo_counter");
    }


    @Timed("demo_timer")
    @GetMapping("/random")
    public double getRandomMetricsData() throws InterruptedException {
        demoCounter.increment();
        TimeUnit.MILLISECONDS.sleep((long) demoCounter.count()*100);
        return getRandomNumberInRange(0, 100);
    }

    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}